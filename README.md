﻿# Mesterséges neuronhálók ELTE IK jegyzet

Ez a repository az ELTE IK Mesterséges Neuronhálók tantárgyához tartozó jegyzeteit tartalmaza. 
Az összeszedett anyag elsősorban Dr. Lőrincz András diasora, illetve leadott órái alapján, másodsorban egyéb intereneten hozzáférhető anyagok alapján készült.

A jegyzet bárki számára elérhető, és módosítható. Aki tehát kiegészítéseket, javításokat szeretne eszközölni, nyugodtan tegye meg. 
Aki a módosításhoz nem érez elég bátorságot, de észrevételei vannak, bátran várjuk Issue formájában (Bitbucket account nélkül is lehet).

Az anyag LaTeX formátumú, a legfrissebb verzióról automatikusan PDF dokumentumok készülnek, melyek a Downloads menüben letölthetők.